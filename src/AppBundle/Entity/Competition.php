<?php

namespace AppBundle\Entity;


use AppBundle\Form\Type\CompetitionFormType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Competition
 * @package AppBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name = "competition")
 */
class Competition extends AbstractEntity
{

    /**
     * @var string
     * @ORM\Column(name = "caption", type = "string")
     */
    protected $caption;

    /**
     * @var string
     * @ORM\Column(name = "description", type = "text", nullable=true)
     */
    protected $description;

    /**
     * @var Kind
     * @ORM\ManyToOne(targetEntity = "AppBundle\Entity\Kind", inversedBy = "competitions")
     * @ORM\JoinColumn(name = "kindid", referencedColumnName = "id")
     */
    protected $kind;

    /**
     * @var Type
     * @ORM\ManyToOne(targetEntity = "AppBundle\Entity\Type", inversedBy = "competitions")
     * @ORM\JoinColumn(name = "typeid", referencedColumnName = "id")
     */
    protected $type;

    /**
     * @var RateSystem
     * @ORM\ManyToOne(targetEntity = "AppBundle\Entity\RateSystem", inversedBy = "competitions")
     * @ORM\JoinColumn(name = "rateid", referencedColumnName = "id")
     */
    protected $ratesystem;

    /**
     * @var boolean
     * @ORM\Column(name = "active", type = "boolean")
     */
    protected $active;

    /**
     * @var \DateTime
     * @ORM\Column(name = "beginat", type = "datetime", nullable=true)
     */
    protected $beginat;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity = "AppBundle\Entity\User", inversedBy = "competitions")
     * @ORM\JoinColumn(name = "authorid", referencedColumnName = "id")
     */
    protected $author;

    /**
     * @var integer
     * @ORM\Column(name = "numpart", type = "integer", nullable=true)
     */
    protected $numpart;

    /**
     * @var ArrayCollection|Competitor[]
     * @ORM\OneToMany(targetEntity = "AppBundle\Entity\Competitor", mappedBy = "competition", cascade = {"persist", "remove"})
     */
    protected $competitors;
    

    public function __construct()
    {
        parent::__construct();
        $this->active = false;
        $this->competitors = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * @param string $caption
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return Kind
     */
    public function getKind()
    {
        return $this->kind;
    }

    /**
     * @param Kind $kind
     */
    public function setKind($kind)
    {
        $this->kind = $kind;
    }

    /**
     * @return Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param Type $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return RateSystem
     */
    public function getRatesystem()
    {
        return $this->ratesystem;
    }

    /**
     * @param RateSystem $ratesystem
     */
    public function setRatesystem($ratesystem)
    {
        $this->ratesystem = $ratesystem;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return \DateTime
     */
    public function getBeginat()
    {
        return $this->beginat;
    }

    /**
     * @param \DateTime $beginat
     */
    public function setBeginat($beginat)
    {
        $this->beginat = $beginat;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return int
     */
    public function getNumpart()
    {
        return $this->numpart;
    }

    /**
     * @param int $numpart
     */
    public function setNumpart($numpart)
    {
        $this->numpart = $numpart;
    }

    /**
     * @return Competitor[]|ArrayCollection
     */
    public function getCompetitors()
    {
        return $this->competitors;
    }

    /**
     * @param Competitor[]|ArrayCollection $competitors
     */
    public function setCompetitors($competitors)
    {
        $this->competitors = $competitors;
    }

    /**
     * @return Competitor[]|ArrayCollection
     */
    public function getActiveCompetitors()
    {
        $competitors = new ArrayCollection();
        foreach ($this->competitors as $competitor) {
            if ($competitor->getStatus()->getId() == 2) {
                $competitors->add($competitor);
            }
        }

        return $competitors;
    }

    /**
     * @param Competitor[]|ArrayCollection $competitors
     */
    public function setActiveCompetitors($competitors)
    {
        $this->competitors = $competitors;
    }
}
