<?php

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Kind
 * @package AppBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name = "kind")
 */
class Kind extends AbstractEntity
{

    /**
     * @var string
     * @ORM\Column(name = "caption", type = "string")
     */
    protected $caption;

    /**
     * @var ArrayCollection|Competition[]
     * @ORM\OneToMany(targetEntity = "AppBundle\Entity\Competition", mappedBy = "kind", cascade = {"persist", "remove"})
     */
    protected $competitions;
    

    public function __construct()
    {
        parent::__construct();
        $this->competitions = new ArrayCollection();

    }

    /**
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * @param string $caption
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;
    }

    /**
     * @return Competition[]|ArrayCollection
     */
    public function getCompetitions()
    {
        return $this->competitions;
    }

    /**
     * @param Competition[]|ArrayCollection $competitions
     */
    public function setCompetitions($competitions)
    {
        $this->competitions = $competitions;
    }

}
