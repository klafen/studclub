<?php

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Competitor
 * @package AppBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name = "competitor")
 */
class Competitor extends AbstractEntity
{
    /**
     * @var Competition
     * @ORM\ManyToOne(targetEntity = "AppBundle\Entity\Competition", inversedBy = "competitors")
     * @ORM\JoinColumn(name = "competitionid", referencedColumnName = "id")
     */
    protected $competition;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity = "AppBundle\Entity\User", inversedBy = "competitors")
     * @ORM\JoinColumn(name = "usrid", referencedColumnName = "id")
     */
    protected $user;

    /**
     * @var integer
     * @ORM\Column(name = "place", type = "integer", nullable=true)
     */
    protected $place;

    /**
     * @var string
     * @ORM\Column(name = "result", type = "string", nullable=true)
     */
    protected $result;

    /**
     * @var Status
     * @ORM\ManyToOne(targetEntity = "AppBundle\Entity\Status", inversedBy = "competitors")
     * @ORM\JoinColumn(name = "statusid", referencedColumnName = "id")
     */
    protected $status;

    public function __construct()
    {
        parent::__construct();


    }

    /**
     * @return Competition
     */
    public function getCompetition()
    {
        return $this->competition;
    }

    /**
     * @param Competition $competition
     */
    public function setCompetition($competition)
    {
        $this->competition = $competition;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param int $place
     */
    public function setPlace($place)
    {
        $this->place = $place;
    }

    /**
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param string $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @return Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param Status $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


}
