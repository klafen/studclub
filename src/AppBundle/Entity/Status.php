<?php

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Status
 * @package AppBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name = "status")
 */
class Status extends AbstractEntity
{

    /**
     * @var string
     * @ORM\Column(name = "caption", type = "string")
     */
    protected $caption;

    /**
     * @var ArrayCollection|Competitor[]
     * @ORM\OneToMany(targetEntity = "AppBundle\Entity\Competitor", mappedBy = "status", cascade = {"persist", "remove"})
     */
    protected $competitors;
    

    public function __construct()
    {
        parent::__construct();
        $this->competitors = new ArrayCollection();

    }

    /**
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * @param string $caption
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;
    }

    /**
     * @return Competitor[]|ArrayCollection
     */
    public function getCompetitors()
    {
        return $this->competitors;
    }

    /**
     * @param Competitor[]|ArrayCollection $competitors
     */
    public function setCompetitors($competitors)
    {
        $this->competitors = $competitors;
    }


}
