<?php
/**
 * Created by PhpStorm.
 * User: Mixa
 * Date: 02.04.2017
 * Time: 19:32
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class User
 * @package AppBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name = "users")
 */
class User extends AbstractEntity implements UserInterface, \Serializable
{

    /**
     * @var string
     * @ORM\Column(name = "password", type = "string")
     */
    protected $password;

    /**
     * @var ArrayCollection|Role[]
     * @ORM\ManyToMany(targetEntity = "AppBundle\Entity\Role", inversedBy = "users")
     * @ORM\JoinTable(name = "user_roles",
     *   joinColumns = {@ORM\JoinColumn(name = "userid", referencedColumnName = "id")},
     *   inverseJoinColumns = {@ORM\JoinColumn(name = "roleid", referencedColumnName = "id")}
     * )
     */
    protected $roles;

    /**
     * @var string
     * @ORM\Column(name = "salt", type = "string")
     */
    protected $salt;

    /**
     * @var string
     * @ORM\Column(name = "username", type = "string", unique = true)
     */
    protected $username;

    /**
     * @var string
     * @ORM\Column(name = "userfio", type = "string")
     */
    protected $userfio;

    /**
     * @var string
     * @ORM\Column(name = "email", type = "string")
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(name = "phone", type = "string", nullable=true)
     */
    protected $phone;

    /**
     * @var boolean
     * @ORM\Column(name = "deleted", type = "boolean")
     */
    protected $deleted;



    public function __construct()
    {
        parent::__construct();
        $this->roles = new ArrayCollection();
        $this->salt = self::generateSalt();
        $this->deleted = false;
    }

    public function eraseCredentials()
    {
        return $this;
    }


    /**
     * @return string
     */
    public static function generateSalt()
    {
        $symbols = '0123456789abcdef';
        $salt = '';

        foreach (range(1, 32) as $i) $salt .= $symbols[mt_rand(0, 15)];

        return $salt;
//        return openssl_random_pseudo_bytes(32);
    }


    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return array|Role[]
     */
    public function getRoles()
    {
        return $this->roles->toArray();
    }

    /**
     * @return ArrayCollection|Role[]
     */
    public function getRolesCollection()
    {
        return $this->roles;
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    public function serialize()
    {
        return serialize(array(
            'id' => $this->id,
            'username' => $this->username,
            'password' => $this->password,
            'salt' => $this->salt
        ));
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    public function setRoles($roles)
    {
        $this->roles->clear();

        foreach ($roles as $role) $this->roles->add($role);

        return $this;
    }

    /**
     * @param string $salt
     * @return $this
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    public function unserialize($serialized)
    {
        $unserialized = unserialize($serialized);
        $this->id = $unserialized['id'];
        $this->username = $unserialized['username'];
        $this->password = $unserialized['password'];
        $this->salt = $unserialized['salt'];

        return $this;
    }

    /**
     * @return boolean
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param boolean $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getUserfio()
    {
        return $this->userfio;
    }

    /**
     * @param string $userfio
     */
    public function setUserfio($userfio)
    {
        $this->userfio = $userfio;
    }

}