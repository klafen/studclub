<?php

namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use AppBundle\Form\Type\AbstractEntityFormType;
use Symfony\Component\Form\FormBuilderInterface;



class CompetitionResultFormType extends AbstractEntityFormType
{
    public function __construct()
    {
        parent::__construct('competition', 'Competition');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('activecompetitors', 'collection', array('label' => 'Участники',
            'type' => new CompetitorFormType(),
            'allow_add' => false,
            'allow_delete' => false,
            'delete_empty' => true
        ));
    }

}