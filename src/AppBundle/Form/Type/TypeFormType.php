<?php

namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use AppBundle\Form\Type\AbstractEntityFormType;
use Symfony\Component\Form\FormBuilderInterface;



class TypeFormType extends AbstractEntityFormType
{
    public function __construct()
    {
        parent::__construct('type', 'Type');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('caption', 'text', array('label' => 'Название', 'required'=>true))
                ->add('description', 'textarea', array('label' => 'Описание', 'required'=>true));
    }

}