<?php

namespace AppBundle\Form\Type;

use AppBundle\Form\Type\AbstractEntityFormType;
use Symfony\Component\Form\FormBuilderInterface;

class RemindPasswordFormType extends AbstractEntityFormType
{
    public function __construct()
    {
        parent::__construct('remindpassword', 'User');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', array('label' => 'Электронная почта','required' => true, 'mapped'=> false));
    }
} 