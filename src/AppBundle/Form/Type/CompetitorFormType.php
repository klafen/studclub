<?php

namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use AppBundle\Form\Type\AbstractEntityFormType;
use Symfony\Component\Form\FormBuilderInterface;



class CompetitorFormType extends AbstractEntityFormType
{
    public function __construct()
    {
        parent::__construct('competitor', 'Competitor');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('user', 'entity', array('label' => 'Участник', 'disabled'=>true, 'class' => 'AppBundle\Entity\User','property' => 'userfio'))
                ->add('place', 'integer', array('label' => 'Место', 'required'=>true))
                ->add('result', 'number', array('label' => 'Результат', 'required'=>true));
    }

}