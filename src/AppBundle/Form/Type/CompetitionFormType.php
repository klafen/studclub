<?php

namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use AppBundle\Form\Type\AbstractEntityFormType;
use Symfony\Component\Form\FormBuilderInterface;



class CompetitionFormType extends AbstractEntityFormType
{
    public function __construct()
    {
        parent::__construct('competition', 'Competition');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('caption', 'text', array('label' => 'Название', 'required'=>true))
            ->add('description', 'textarea', array('label' => 'Описание', 'required'=>true))
            ->add('kind', 'entity', array('label' => 'Вид спорта', 'required' => true,
                'class' => 'AppBundle\Entity\Kind',
                'property' => 'caption'))
            ->add('type', 'entity', array('label' => 'Вид соревнования', 'required' => true,
                'class' => 'AppBundle\Entity\Type',
                'property' => 'caption'))
            ->add('ratesystem', 'entity', array('label' => 'Система оценивания', 'required' => true,
                'class' => 'AppBundle\Entity\RateSystem',
                'property' => 'caption'))
            ->add('beginat', 'datetime', array('label' => 'Дата и время проведения', 'required' => true))
            ->add('numpart', 'integer', array('label' => 'Количество участников', 'required' => false));
    }

}