<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Department;
use AppBundle\Entity\Kind;
use AppBundle\Entity\ProductCategory;
use AppBundle\Entity\RateSystem;
use AppBundle\Entity\Type;
use AppBundle\Form\Type\DepartmentFormType;
use AppBundle\Form\Type\KindFormType;
use AppBundle\Form\Type\ProductCategoryFilterFormType;
use AppBundle\Form\Type\ProductCategoryFormType;
use AppBundle\Form\Type\ProductFormType;
use AppBundle\Entity\Role;
use AppBundle\Entity\Product;
use AppBundle\Form\Type\RateSystemFormType;
use AppBundle\Form\Type\TypeFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Config;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class SpravController extends InitializableController
{

    /**
     * @return RedirectResponse|Response
     * @Config\Route("/", name = "site_sprav_index")
     */
    public function indexAction()
    {

        $this->navigation = array('active' => 'sprav');
        return $this->render('AppBundle:Sprav:index.html.twig');
    }
    
    /**
     * @return RedirectResponse|Response
     * @Config\Route("/kinds", name = "site_kinds_index")
     */
    public function kindindexAction()
    {

        
        $kinds = $this->getRepository('Kind')->createQueryBuilder('k')
            ->orderBy('k.caption')->getQuery()->getResult();

        $this->view['kinds'] = $kinds;

        $this->navigation = array('active' => 'sprav');
        return $this->render('AppBundle:Sprav:kindindex.html.twig');
    }

    /**
     * @return RedirectResponse|Response
     * @Config\Route("/kinds/add", name = "site_kinds_add")
     */
    public function kindaddAction()
    {
        $kind = new Kind();
        $form = $this->createForm(new KindFormType(), $kind);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
                $this->manager->persist($kind);
                $this->manager->flush();

                $this->addNotice('success',
                    'sprav.html.twig',
                    array('notice' => 'kind_added', 'caption' => $kind->getCaption())
                );

                return $this->redirectToRoute('site_kinds_index');
            }


        $this->forms['kind'] = $form->createView();
        $this->view['kind'] = null;
        $this->navigation = array('active' => 'sprav');
        return $this->render('AppBundle:Sprav:kind.html.twig');
    }

    /**
     * @param Kind $kind
     * @return RedirectResponse|Response
     * @Config\Route("/kinds/{kind}/edit", name = "site_kinds_edit")
     * @Config\ParamConverter("kind", options = {"mapping": {"kind": "id"}})
     */
    public function kindeditAction(Kind $kind)
    {
        $form = $this->createForm(new KindFormType(), $kind);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
                $this->manager->persist($kind);
                $this->manager->flush();
                $this->addNotice('success',
                    'sprav.html.twig',
                    array('notice' => 'kind_changed', 'caption' => $kind->getCaption())
                );

                return $this->redirectToRoute('site_kinds_index');
            }


        $this->forms['kind'] = $form->createView();
        $this->view['kind'] = $kind;
        $this->navigation = array('active' => 'sprav');
        return $this->render('AppBundle:Sprav:kind.html.twig');
    }

    /**
     * @param Kind $kind
     * @return RedirectResponse|Response
     * @Config\Route("/kinds/{kind}/remove", name = "site_kinds_remove")
     * @Config\ParamConverter("kind", options = {"mapping": {"kind": "id"}})
     */
    public function kindremoveAction(Kind $kind)
    {
    $this->manager->remove($kind);
    $this->manager->flush();
    $this->addNotice('error',
        'sprav.html.twig',
        array('notice' => 'kind_removed', 'caption' => $kind->getCaption())
    );
        return $this->redirectToRoute('site_kinds_index');
    }

    /**
     * @return RedirectResponse|Response
     * @Config\Route("/types", name = "site_types_index")
     */
    public function typeindexAction()
    {


        $types = $this->getRepository('Type')->createQueryBuilder('t')
            ->orderBy('t.caption')->getQuery()->getResult();

        $this->view['types'] = $types;

        $this->navigation = array('active' => 'sprav');
        return $this->render('AppBundle:Sprav:typeindex.html.twig');
    }

    /**
     * @return RedirectResponse|Response
     * @Config\Route("/types/add", name = "site_types_add")
     */
    public function typeaddAction()
    {
        $type = new Type();
        $form = $this->createForm(new TypeFormType(), $type);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($type);
            $this->manager->flush();

            $this->addNotice('success',
                'sprav.html.twig',
                array('notice' => 'type_added', 'caption' => $type->getCaption())
            );

            return $this->redirectToRoute('site_types_index');
        }


        $this->forms['type'] = $form->createView();
        $this->view['type'] = null;
        $this->navigation = array('active' => 'sprav');
        return $this->render('AppBundle:Sprav:type.html.twig');
    }

    /**
     * @param Type $type
     * @return RedirectResponse|Response
     * @Config\Route("/types/{type}/edit", name = "site_types_edit")
     * @Config\ParamConverter("type", options = {"mapping": {"type": "id"}})
     */
    public function typeeditAction(Type $type)
    {
        $form = $this->createForm(new TypeFormType(), $type);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($type);
            $this->manager->flush();
            $this->addNotice('success',
                'sprav.html.twig',
                array('notice' => 'type_changed', 'caption' => $type->getCaption())
            );

            return $this->redirectToRoute('site_types_index');
        }


        $this->forms['type'] = $form->createView();
        $this->view['type'] = $type;
        $this->navigation = array('active' => 'sprav');
        return $this->render('AppBundle:Sprav:type.html.twig');
    }

    /**
     * @param Type $type
     * @return RedirectResponse|Response
     * @Config\Route("/types/{type}/remove", name = "site_types_remove")
     * @Config\ParamConverter("type", options = {"mapping": {"type": "id"}})
     */
    public function typeremoveAction(Type $type)
    {
        $this->manager->remove($type);
        $this->manager->flush();
        $this->addNotice('error',
            'sprav.html.twig',
            array('notice' => 'type_removed', 'caption' => $type->getCaption())
        );
        return $this->redirectToRoute('site_types_index');
    }

    /**
     * @return RedirectResponse|Response
     * @Config\Route("/ratesystems", name = "site_ratesystems_index")
     */
    public function ratesystemindexAction()
    {
        $ratesystems = $this->getRepository('RateSystem')->createQueryBuilder('rs')
            ->orderBy('rs.caption')->getQuery()->getResult();

        $this->view['ratesystems'] = $ratesystems;

        $this->navigation = array('active' => 'sprav');
        return $this->render('AppBundle:Sprav:ratesystemindex.html.twig');
    }

    /**
     * @return RedirectResponse|Response
     * @Config\Route("/ratesystems/add", name = "site_ratesystems_add")
     */
    public function ratesystemaddAction()
    {
        $ratesystem = new RateSystem();
        $form = $this->createForm(new RateSystemFormType(), $ratesystem);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($ratesystem);
            $this->manager->flush();

            $this->addNotice('success',
                'sprav.html.twig',
                array('notice' => 'ratesystem_added', 'caption' => $ratesystem->getCaption())
            );

            return $this->redirectToRoute('site_ratesystems_index');
        }


        $this->forms['ratesystem'] = $form->createView();
        $this->view['ratesystem'] = null;
        $this->navigation = array('active' => 'sprav');
        return $this->render('AppBundle:Sprav:ratesystem.html.twig');
    }

    /**
     * @param RateSystem $ratesystem
     * @return RedirectResponse|Response
     * @Config\Route("/ratesystems/{ratesystem}/edit", name = "site_ratesystems_edit")
     * @Config\ParamConverter("ratesystem", options = {"mapping": {"ratesystem": "id"}})
     */
    public function ratesystemeditAction(RateSystem $ratesystem)
    {
        $form = $this->createForm(new RateSystemFormType(), $ratesystem);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($ratesystem);
            $this->manager->flush();
            $this->addNotice('success',
                'sprav.html.twig',
                array('notice' => 'ratesystem_changed', 'caption' => $ratesystem->getCaption())
            );

            return $this->redirectToRoute('site_ratesystems_index');
        }


        $this->forms['ratesystem'] = $form->createView();
        $this->view['ratesystem'] = $ratesystem;
        $this->navigation = array('active' => 'sprav');
        return $this->render('AppBundle:Sprav:ratesystem.html.twig');
    }

    /**
     * @param RateSystem $ratesystem
     * @return RedirectResponse|Response
     * @Config\Route("/ratesystems/{ratesystem}/remove", name = "site_ratesystems_remove")
     * @Config\ParamConverter("ratesystem", options = {"mapping": {"ratesystem": "id"}})
     */
    public function ratesystemremoveAction(RateSystem $ratesystem)
    {
        $this->manager->remove($ratesystem);
        $this->manager->flush();
        $this->addNotice('error',
            'sprav.html.twig',
            array('notice' => 'ratesystem_removed', 'caption' => $ratesystem->getCaption())
        );
        return $this->redirectToRoute('site_ratesystems_index');
    }

}
