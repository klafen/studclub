<?php

namespace AppBundle\Controller;

use AppBundle\Form\Type\LoginFormType;
use AppBundle\Form\Type\ProfileFormType;
use AppBundle\Controller\InitializableController;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Form\Type\RegistrationFormType;
use AppBundle\Form\Type\RemindPasswordFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Config;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Security;
require('autoload.php');

class SecurityController extends InitializableController
{
    /**
     * @return RedirectResponse|Response
     * @Config\Route("/login", name = "site_security_login")
     */
    public function loginAction()
    {
        if ($this->authChecker->isGranted(Role::USER)) return $this->redirectToRoute('homepage');

        $error = null;

        if ($this->request->attributes->has(Security::AUTHENTICATION_ERROR))
            $error = $this->request->attributes->get(Security::AUTHENTICATION_ERROR);
        else {
            $error = $this->session->get(Security::AUTHENTICATION_ERROR, null);
            $this->session->remove(Security::AUTHENTICATION_ERROR);
        }

        if (!is_null($error)) {
            $this->addNotice('error', 'security_login.html.twig', array('notice' => 'auth_error'));
        }
        $form = $this->createForm(new LoginFormType(), new User());

        $this->navigation = array('active' => 'login');
        $this->forms = array(
            'login' => $form->createView(),
            'last_username' => $this->session->get(Security::LAST_USERNAME, null)
        );


        return $this->render('AppBundle:Security:login.html.twig');
    }

    /**
     * @throws NotFoundHttpException
     * @Config\Route("/login-check", name = "site_security_login_check")
     */
    public function loginCheckAction()
    {
        throw $this->createNotFoundException();
    }

    /**
     * @throws NotFoundHttpException
     * @Config\Route("/logout", name = "site_security_logout")
     */
    public function logoutAction()
    {
        throw $this->createNotFoundException();
    }

    /**
     * @return RedirectResponse|Response
     * @Config\Route("/profile", name = "site_security_profile")
     */
    public function profileAction()
    {
        $form = $this->createForm(new ProfileFormType(), $this->user);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!is_null($form->get('password')->getData())) {
                /** @var UserPasswordEncoder $encoder */
                $encoder = $this->get('security.password_encoder');
                $this->user->setSalt(User::generateSalt())
                    ->setPassword($encoder->encodePassword($this->user, $this->user->getPassword()));
                $this->manager->persist($this->user);
                $this->manager->flush();
            }

            $this->addNotice('success',
                'security_profile.html.twig',
                array('notice' => 'user_changed')
            );

            return $this->redirectToRoute('homepage');
        }

        $this->forms['profile'] = $form->createView();
        $this->navigation = array('active' => 'profile');
        return $this->render('AppBundle:Security:profile.html.twig');
    }

    //регистрация на сайте
    /**
     * @return Response
     * @Config\Route("/registration", name = "site_general_registration")
     */
    public function registerAction()
    {
        $user = new User();
        $user->getRolesCollection()->add($this->getRepository('Role')->findOneByRole(Role::USER));
        $form = $this->createForm(new RegistrationFormType(), $user);
        $form->handleRequest($this->request);
        //обработка отправки формы
        if ($form->isSubmitted() && $form->isValid()) {
            $valid = true;
            $samesemail = $this->getRepository('User')->createQueryBuilder('u')
                ->select('COUNT(u.id) AS id')
                ->where('u.email = :email')
                ->setParameters(array('email' => $user->getEmail()))
                ->getQuery()->getSingleScalarResult();

            if ($samesemail > 0) {
                $form->get('email')->addError(new FormError('Пользователь с таким email уже существует.'));
                $valid = false;
            }

            $sameslogin = $this->getRepository('User')->createQueryBuilder('u')
                ->select('COUNT(u.id) AS id')
                ->where('u.username = :username')
                ->setParameters(array('username' => $user->getUsername()))
                ->getQuery()->getSingleScalarResult();

            if ($sameslogin > 0) {
                $form->get('username')->addError(new FormError('Пользователь с таким логином уже существует.'));
                $valid = false;
            }
            $recaptcha = new \ReCaptcha\ReCaptcha("6Ld7uyEUAAAAACSENMkv5EtNoniiD7lO5ZSYoao2");
            $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
            if (!($resp->isSuccess())) {
                $valid = false;
                $form->addError(new FormError('Поставьте галочку, что Вы не робот!'));
            }
            if ($valid) {
                /** @var UserPasswordEncoder $encoder */
                $encoder = $this->get('security.password_encoder');
                $userpassword = $user->getPassword();
                $user->setSalt(User::generateSalt())
                    ->setPassword($encoder->encodePassword($user, $user->getPassword()));
                $this->manager->persist($user);
                $this->manager->flush();


                $body = "
                <html>
<body > <div style='background-color: #f5f5f5; width:100%;margin:0px;padding-bottom:10px;min-height:350px;padding-top:20px;'> <br />
<table style='background-color: #fff; color:#333;border-radius:10px;max-width:600px;min-width:400px;margin:auto;padding-left:20px;padding-right:20px;font-size:18px;padding-top:20px;box-shadow: 0 0 10px rgba(0,0,0,0.5);'>
	<tr>
		<td style='text-align:center;font-size:28px;'><b>Студенческий спортивный клуб</b><br /><br /> </td>
	</tr>
	<tr>
		<td style='text-align:left;'>

                    Здравствуйте! <br /><br />
		</td>
	</tr>
	<tr>

		<td style='text-align:left;'>
		<br />
                    Только что Вы зарегистрировались <a href='#'>на сайте студенческого спортивного клуба</a>.<br />
		</td>
	</tr>
	<tr>
		<td style='text-align:left;'>
		<br />
		При регистрации Вы указали следующие данные: <br />
		Имя пользователя: %login%<br />
		Пароль: %password%<br />

		</td>
	</tr>

	<tr>
		<td style='text-align:left;color:#222;font-size:18px;'>
		<br />
		<hr>                
		</td>
	</tr>
</table>
</div>                
                </body>
</html>\"
                ";

                $body = preg_replace('/%login%/',$user->getUsername(), $body); //заменяем выражения
                $body = preg_replace('/%password%/',$userpassword, $body); //заменяем выражения
                $emails = $user->getEmail();
                /** @var \Swift_Mailer $mailer */
                $mailer = $this->get('mailer');
                $message = $mailer->createMessage()
                    ->setSubject('Регистрация на сервисе')
                    ->setFrom(array('noreply@'.$_SERVER['SERVER_NAME'] => 'СтудКлуб'))
                    ->setTo($emails)
                    ->setBody($body, 'text/html');
                $mailer->send($message);

                $token = new UsernamePasswordToken($user, null, 'user_provider', $user->getRoles());
                $this->get('security.context')->setToken($token);
                return $this->redirectToRoute('homepage');
            }
        }
        $this->navigation = array('active' => 'registration');
        $this->forms['user'] = $form->createView();

        return $this->render('AppBundle:Security:registration.html.twig');
    }
//напоминание пароля
    /**
     * @return Response
     * @Config\Route("/remindpassword", name = "site_general_remindpassword")
     */
    public function remindpasswordAction()
    {

        $user = new User();
        $form = $this->createForm(new RemindPasswordFormType(), $user);
        $form->handleRequest($this->request);
//обработка формы
        if ($form->isSubmitted() && $form->isValid()) {
            $valid = true;
            /** @var User $user */
            $user = $this->getRepository('User')->findOneBy(array('email'=>$form->get('email')->getData()));

            if (is_null($user)) {
                $form->get('email')->addError(new FormError('Пользователь с таким email не зарегистрирован.'));
                $valid = false;
            }
            $recaptcha = new \ReCaptcha\ReCaptcha("6Ld7uyEUAAAAACSENMkv5EtNoniiD7lO5ZSYoao2");
            $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
            if (!($resp->isSuccess())) {
                $valid = false;
                $form->addError(new FormError('Поставьте галочку, что Вы не робот!'));
            }

            if ($valid) {
                /** @var UserPasswordEncoder $encoder */
                $encoder = $this->get('security.password_encoder');
                $userpassword = substr (md5(uniqid(rand(),true)),rand(1,4),rand(6,8));
                $user->setSalt(User::generateSalt())
                    ->setPassword($encoder->encodePassword($user, $userpassword));
                $this->manager->persist($user);
                $this->manager->flush();


                $body = "
                <html>
<body > <div style='background-color: #f5f5f5; width:100%;margin:0px;padding-bottom:10px;min-height:350px;padding-top:20px;'> <br />
<table style='background-color: #fff; color:#333;border-radius:10px;max-width:600px;min-width:400px;margin:auto;padding-left:20px;padding-right:20px;font-size:18px;padding-top:20px;box-shadow: 0 0 10px rgba(0,0,0,0.5);'>
	<tr>
		<td style='text-align:center;font-size:28px;'><b>Студенческий спортивный клуб</b><br /><br /> </td>
	</tr>
	<tr>
		<td style='text-align:left;'>

                    Здравствуйте! <br /><br />
		</td>
	</tr>
	<tr>

		<td style='text-align:left;'>
		<br />
                    Только что Вы запросили новый пароль <a href='#'>на сайте студенческого спортивного клуба</a>.<br />
		</td>
	</tr>
	<tr>
		<td style='text-align:left;'>
		<br />
		Ваши данные для входа на сайт: <br />
		Имя пользователя: %login%<br />
		Пароль: %password%<br />

		</td>
	</tr>

	<tr>
		<td style='text-align:left;color:#222;font-size:18px;'>
		<br />
		<hr>                
		</td>
	</tr>
</table>
</div>                
                </body>
</html>\"
                ";

                $body = preg_replace('/%login%/',$user->getUsername(), $body); //заменяем выражения
                $body = preg_replace('/%password%/',$userpassword, $body); //заменяем выражения
                $emails = $user->getEmail();
                /** @var \Swift_Mailer $mailer */
                $mailer = $this->get('mailer');
                $message = $mailer->createMessage()
                    ->setSubject('Восстановление пароля')
                    ->setFrom(array('noreply@'.$_SERVER['SERVER_NAME'] => 'СтудКлуб'))
                    ->setTo($emails)
                    ->setBody($body, 'text/html');
                $mailer->send($message);

                return $this->redirectToRoute('site_general_remindpasswordsuccess');
            }
        }
        $this->navigation = array('active' => 'remindpassword');
        $this->forms['user'] = $form->createView();

        return $this->render('AppBundle:Security:remindpassword.html.twig');
    }
//страница с инстукцией по проверке почты, на которую был выслан новый пароль
    /**
     * @return Response
     * @Config\Route("/remindpasswordok", name = "site_general_remindpasswordsuccess")
     */
    public function remindpasswordsuccessAction()
    {
       

        $this->navigation = array('active' => 'remindpassword');

        return $this->render('AppBundle:Security:remindpasswordok.html.twig');
    }
    
}
