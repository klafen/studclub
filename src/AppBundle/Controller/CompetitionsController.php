<?php

namespace AppBundle\Controller;
use AppBundle\Entity\Competition;
use AppBundle\Entity\Competitor;
use AppBundle\Entity\Status;
use AppBundle\Form\Type\CompetitionFilterFormType;
use AppBundle\Form\Type\CompetitionFormType;
use AppBundle\Entity\Role;
use AppBundle\Form\Type\CompetitionResultFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Config;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Security;

class CompetitionsController extends InitializableController
{
    /**
     * @return RedirectResponse|Response
     * @Config\Route("/competitions/index/{pagenum}", name = "site_competitions_index", defaults={ "pagenum": "1"})
     */
    public function indexAction($pagenum=1)
    {
        $form=$this->createForm(new CompetitionFilterFormType());
        $caption = null;
        $kind=null;
        $form->handleRequest($this->request);

        $competitionsquery = $this->getRepository('Competition')->createQueryBuilder('c')
            ->leftJoin('c.kind', 'k')
            ->orderBy('c.id', 'DESC');

        $competitionscountquery = $this->getRepository('Competition')->createQueryBuilder('c')
            ->select('COUNT(DISTINCT c.id)')
            ->orderBy('c.id', 'DESC');

        if ($form->isSubmitted() && $form->isValid()) {
            $caption = $form->get('caption')->getData();
            $kind = $form->get('kind')->getData();
        }

        if (!empty($caption)) {
            $competitionsquery->andWhere('LOWER(c.caption) LIKE LOWER(:caption) ')->setParameter('caption', '%' . trim($caption) . '%');
            $competitionscountquery->andWhere('LOWER(c.caption) LIKE LOWER(:caption) ')->setParameter('caption', '%' . trim($caption) . '%');
        }

        if (!empty($kind)) {
            $competitionsquery->andWhere('k.id = :kind')->setParameter('kind', $kind);
            $competitionscountquery->andWhere('k.id = :kind')->setParameter('kind', $kind);
        }

        if (!($this->authChecker->isGranted(Role::ADMIN))) {
            $competitionsquery->andWhere('c.active = :active')->setParameter('active', true);
            $competitionscountquery->andWhere('c.active = :active')->setParameter('active', true);
        }

        $count=$competitionscountquery->getQuery()->getSingleScalarResult();

        $pages = floor($count / 20) + ($count % 20 > 0 ? 1 : 0);
        if ($pages < 1) $pages = 1;
        if ($pagenum > $pages) $pagenum = $pages;
        $competitions = $competitionsquery->setFirstResult(($pagenum - 1) * 20)
            ->setMaxResults(20)
            ->getQuery()->getResult();



        $this->view['competitions'] = $competitions;
        $this->view['form'] = $form->createView();
        $this->view['page']=$pagenum;
        $this->view['pages']=$pages;
        $this->navigation = array('active' => 'competitions');
        return $this->render('AppBundle:Competitions:index.html.twig');
    }

    /**
     * @return RedirectResponse|Response
     * @Config\Route("/competitions/add", name = "site_competitions_add")
     */
    public function addAction()
    {
        if (!($this->authChecker->isGranted(Role::ADMIN))) return $this->redirectToRoute('homepage');
        
        $competition = new Competition();
        $form = $this->createForm(new CompetitionFormType(), $competition);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
                $this->manager->persist($competition);
                $this->manager->flush();

                $this->addNotice('success',
                    'competitions.html.twig',
                    array('notice' => 'added', 'caption' => $competition->getCaption())
                );

                return $this->redirectToRoute('site_competitions_edit', array('competition'=>$competition->getId(), 'step'=>1));
            }

        $this->view['competition']=null;
        $this->forms['competition'] = $form->createView();
        $this->navigation = array('active' => 'competitions');
        return $this->render('AppBundle:Competitions:competition1.html.twig');
    }

    /**
     * @param Competition $competition
     * @return RedirectResponse|Response
     * @Config\Route("/competitions/{competition}/edit/{step}", name = "site_competitions_edit", defaults={ "step": "1"})
     * @Config\ParamConverter("competition", options = {"mapping": {"competition": "id"}})
     */
    public function editAction(Competition $competition, $step=1)
    {
        if ($step > 4) {
            $step = 4;
        }
        switch ($step) {
            //общие данные соревнования
            case 1:
                $form = $this->createForm(new CompetitionFormType(), $competition);
                $form->handleRequest($this->request);
                if ($form->isSubmitted() && $form->isValid()) {
                    $this->manager->persist($competition);
                    $this->manager->flush();
                    $this->addNotice('success',
                        'competitions.html.twig',
                        array('notice' => 'changed', 'caption' => $competition->getCaption())
                    );
                    return $this->redirectToRoute('site_competitions_edit', array('competition'=>$competition->getId(), 'step'=>1));
                }
                $this->forms['competition'] = $form->createView();
                break;

            case 2:
                //список участников
                $competitors = $this->getRepository('Competitor')->createQueryBuilder('c')
                    ->where('c.competition = :competition')
                    ->orderBy('c.status')
                    ->addOrderBy('c.createdAt')
                    ->setParameters(array('competition'=>$competition))
                    ->getQuery()->getResult();
                $this->view['competitors'] = $competitors;
                break;
            case 3:
                //результаты
                $competitors = $this->getRepository('Competitor')->createQueryBuilder('c')
                    ->where('c.competition = :competition')
                    ->andWhere('c.status = :status')
                    ->andWhere('c.place IS NOT NULL')
                    ->orderBy('c.place')
                    ->addOrderBy('c.createdAt')
                    ->setParameters(array('competition'=>$competition, 'status'=>2))
                    ->getQuery()->getResult();
                $this->view['competitors'] = $competitors;
                break;
            case 4:
                $form = $this->createForm(new CompetitionResultFormType(), $competition);
                $form->handleRequest($this->request);
                    if ($form->isSubmitted() && $form->isValid()) {
                        $this->manager->persist($competition);
                        $this->manager->flush();
                        $this->addNotice('success',
                            'competitions.html.twig',
                            array('notice' => 'changed', 'caption' => $competition->getCaption())
                        );
                        return $this->redirectToRoute('site_competitions_edit', array('competition'=>$competition->getId(), 'step'=>3));
                    }
                    $this->forms['competition'] = $form->createView();
                break;

        }

        $this->view['competition'] = $competition;
        $this->navigation = array('active' => 'competitions');
        return $this->render('AppBundle:Competitions:competition'.$step.'.html.twig');
    }

    /**
     * @param Competition $competition
     * @return Response
     * @Config\Route("/competitions/{competition}/publish", name = "site_competitions_publish")
     * @Config\ParamConverter("competition", options = {"mapping": {"competition": "id"}})
     */
    public function publishAction(Competition $competition)
    {
        $competition->setActive(true);
        $this->manager->persist($competition);
        $this->manager->flush();
        $this->addNotice('info',
            'competitions.html.twig',
            array('notice' => 'publish', 'caption' => $competition->getCaption())
        );
        return $this->redirectToRoute('site_competitions_index');
    }

    /**
     * @param Competition $competition
     * @return Response
     * @Config\Route("/competitions/{competition}/unpublish", name = "site_competitions_unpublish")
     * @Config\ParamConverter("competition", options = {"mapping": {"competition": "id"}})
     */
    public function unpublishAction(Competition $competition)
    {
        $competition->setActive(false);
        $this->manager->persist($competition);
        $this->manager->flush();
        $this->addNotice('info',
            'competitions.html.twig',
            array('notice' => 'unpublish', 'caption' => $competition->getCaption())
        );
        return $this->redirectToRoute('site_competitions_index');
    }

    /**
     * @param Competitor $competitor
     * @return Response
     * @Config\Route("/competitors/{competitor}/changestatus/{status}", name = "site_competititor_change_status")
     * @Config\ParamConverter("competition", options = {"mapping": {"competition": "id"}})
     */
    public function changestatusAction(Competitor $competitor, Status $status)
    {
        $competitor->setStatus($status);
        $this->manager->persist($competitor);
        $this->manager->flush();
        if ($status->getId() ==2 ) {
            $this->addNotice('info',
                'competitions.html.twig',
                array('notice' => 'apply', 'competitor' => $competitor->getUser()->getUserfio())
            );
        }
        else {
            $this->addNotice('warning',
                'competitions.html.twig',
                array('notice' => 'cancel', 'competitor' => $competitor->getUser()->getUserfio())
            );
        }
       
        return $this->redirectToRoute('site_competitions_edit', array ('competition'=>$competitor->getCompetition()->getId(), 'step'=>2));
    }

    /**
     * @param Competition $competition
     * @return Response
     * @Config\Route("/competitions/{competition}/addcompetitor", name = "site_competitions_addcompetitor")
     * @Config\ParamConverter("competition", options = {"mapping": {"competition": "id"}})
     */
    public function addcompetitorAction(Competition $competition)
    {
        /** @var Status $status */
        $status = $this->getRepository('Status')
            ->findOneBy(array('id' => 1));

        $competitor = new Competitor();
        $competitor->setCompetition($competition);
        $competitor->setUser($this->user);
        $competitor->setStatus($status);
        $this->manager->persist($competitor);
        $this->manager->flush();
        $this->addNotice('success',
            'competitions.html.twig',
            array('notice' => 'addcompetitor', 'caption' => $competition->getCaption())
        );
        return $this->redirectToRoute('site_competitions_edit', array ('competition'=>$competitor->getCompetition()->getId(), 'step'=>2));
    }

    /**
     * @return RedirectResponse|Response
     * @Config\Route("/requests", name = "site_my_requests")
     * @Config\ParamConverter("competition", options = {"mapping": {"competition": "id"}})
     */
    public function mycompetitorsAction()
    {
        //список заявок
        $competitors = $this->getRepository('Competitor')->createQueryBuilder('c')
            ->where('c.user = :user')
            ->orderBy('c.status')
            ->addOrderBy('c.createdAt')
            ->setParameters(array('user'=>$this->user))
            ->getQuery()->getResult();
        $this->view['competitors'] = $competitors;         

        $this->navigation = array('active' => 'requests');
        return $this->render('AppBundle:Competitions:requests.html.twig');
    }

    /**
     * @return RedirectResponse|Response
     * @Config\Route("/competitions/my", name = "site_my_competitions")
     * @Config\ParamConverter("competition", options = {"mapping": {"competition": "id"}})
     */
    public function mycompetitionsAction()
    {
        /** @var Status $status */
        $status = $this->getRepository('Status')
            ->findOneBy(array('id' => 2));
        
        //список соревнований
        $competitions = $this->getRepository('Competition')->createQueryBuilder('c')
            ->select ('c.id, c.caption, c.beginat, cr.place, cr.result')
            ->leftJoin('c.competitors', 'cr')
            ->where('cr.user = :user')
            ->andWhere('cr.status = :status')
            ->andWhere('cr.place IS NOT NULL')
            ->addOrderBy('c.beginat')
            ->setParameters(array('user'=>$this->user, 'status'=>$status))
            ->getQuery()->getResult();
        $this->view['competitions'] = $competitions;

        $this->navigation = array('active' => 'mycompetitions');
        return $this->render('AppBundle:Competitions:mycompetitions.html.twig');
    }

}
